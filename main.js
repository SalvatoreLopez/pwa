// Service Worker
if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
    var deferredPrompt;
    navigator.serviceWorker.register('./sw.js')
        .then((res) => console.log("ServiceWorker cargado correctamente", res))
        .catch((err) => console.log("ServiceWorker no se ha podido registrar"));

   
        const btnSave = document.getElementById("btnSave");
        window.addEventListener('beforeinstallprompt',  (e) => {
            console.log('beforeinstallprompt Event fired');
            e.preventDefault();

            // Stash the event so it can be triggered later.
            deferredPrompt = e;
            btnSave.classList.remove('d-none');
           
        });
        btnSave.addEventListener('click', function() {
            if(deferredPrompt !== undefined) {
              // The user has had a postive interaction with our app and Chrome
              // has tried to prompt previously, so let's show the prompt.
              deferredPrompt.prompt();
          
              // Follow what the user has done with the prompt.
              deferredPrompt.userChoice.then(function(choiceResult) {
          
                console.log(choiceResult.outcome);
          
                if(choiceResult.outcome == 'dismissed') {
                  console.log('User cancelled home screen install');
                }
                else {
                  console.log('User added to home screen');
                }
          
                // We no longer need the prompt.  Clear it up.
                deferredPrompt = null;
              });
            }
          });

        window.addEventListener('appinstalled', (event) => {
            console.log('installed',event);
        });

     
    });
}
else {
    console.log("no puedes usar los serviceWorker en tu navegador");
}

//Scroll suavizado
$(document).ready(function () {
    $("#menu a").click(function (e) {
        e.preventDefault();
        $("html, body").animate({
            scrollTop: $($(this).attr('href')).offset().top
        });

        return false;
    });
});